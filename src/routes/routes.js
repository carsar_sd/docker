// libs
import VueRouter from 'vue-router';

// components
import Home from '../pages/Home';
import About from '../pages/About';
import Car from '../pages/Car';
import Cars from '../pages/Cars';
import Contacts from '../pages/Contacts';


export default new VueRouter({
    routes: [
        {
            path: '',
            component: Home,
        },
        {
            path: '/about',
            component: About,
        },
        {
            path: '/cars',
            component: Cars,
        },
        {
            path: '/cars/:id',
            component: Car,
        },
        {
            path: '/contacts',
            component: Contacts,
        },
    ],
    mode: 'history'
});