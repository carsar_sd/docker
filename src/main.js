// libs
import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueRouter from 'vue-router';

// components
import App from './App.vue';

// routes
import router from './routes/routes'

// styles
import './assets/scss/style.scss';


Vue.use(Vuelidate);
Vue.use(VueRouter);


new Vue({
    el: '#app',
    render: h => h(App),
    router,
});
