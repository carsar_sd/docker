# ! help for myself !
# docker rmi $(docker images -a -q)
# docker rmi image_id
# docker rmi -f image_id
# docker build -t carsar/vuejs:route .
# docker push carsar/vuejs
# docker run -it --rm -p 80:80 carsar/vuejs:route
# docker-compose up -d
# http://localhost/

# этап сборки (build stage)
FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build

# этап production (production-stage)
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]

EXPOSE 80